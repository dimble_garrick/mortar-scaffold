module gitlab.com/scaffold

go 1.16

require (
	github.com/alecthomas/kong v0.2.17
	github.com/go-masonry/bjaeger v1.0.8
	github.com/go-masonry/bprometheus v1.0.8
	github.com/go-masonry/bviper v1.0.8
	github.com/go-masonry/bzerolog v1.0.8
	github.com/go-masonry/mortar v1.0.11
	github.com/go-masonry/mortar-template v0.0.0-20210206091052-af631bd29547
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0
	github.com/opentracing/opentracing-go v1.2.0
	go.uber.org/fx v1.13.1
	google.golang.org/genproto v0.0.0-20210617175327-b9e0b3197ced
	google.golang.org/grpc v1.38.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.26.0
)
